# Example Makefile for ROSE users
# This makefile is provided as an example of how to use ROSE when ROSE is
# installed (using "make install").  This makefile is tested as part of the
# "make distcheck" rule (run as part of tests before any CVS checkin).
# The test of this makefile can also be run by using the "make installcheck"
# rule (run as part of "make distcheck").


# Location of include directory after "make install"
ROSE_INCLUDE_DIR = /home/legup/compileRose/include/rose

# Location of Boost include directory
BOOST_CPPFLAGS = -I/home/legup/compileBoost/include/

# Location of Dwarf include and lib (if ROSE is configured to use Dwarf)
ROSE_DWARF_INCLUDES = 
ROSE_DWARF_LIBS_WITH_PATH = 
ROSE_INCLUDE_DIR += $(ROSE_DWARF_INCLUDES)
ROSE_LIBS += $(ROSE_DWARF_LIBS_WITH_PATH)

CC                    = gcc
CXX                   = g++
CPPFLAGS              = 
#CXXCPPFLAGS           = @CXXCPPFLAGS@
CXXFLAGS              =  -g -O2 -Wall
LDFLAGS               = 

# Location of library directory after "make install"
ROSE_LIB_DIR = /home/legup/compileRose/lib

ROSE_LIBS = $(ROSE_LIB_DIR)/librose.la

ROSE_SOURCE_DIR = /home/legup/compileRose/eop-insertion

# Default make rule to use
all: addEOP
	@if [ x$${ROSE_IN_BUILD_TREE:+present} = xpresent ]; then echo "ROSE_IN_BUILD_TREE should not be set" >&2; exit 1; fi

# Example suffix rule for more experienced makefile users
# .C.o:
#	g++ -c -I$(ROSE_INCLUDE_DIR) -o $@ $(@:.o=.C)

# Compile using the C file
addEOP.lo:
	/bin/bash /home/legup/compileRose/libtool --mode=compile $(CXX) $(CXXFLAGS)  $(CPPFLAGS) -I$(ROSE_INCLUDE_DIR) $(BOOST_CPPFLAGS) -c -o addEOP.lo $(ROSE_SOURCE_DIR)/addEOP.C

addEOP: addEOP.lo
	/bin/bash /home/legup/compileRose/libtool --mode=link $(CXX) $(CXXFLAGS) $(LDFLAGS) -o addEOP addEOP.lo $(ROSE_LIBS) -lboost_system  -lboost_iostreams

# Rule used by make installcheck to verify correctness of installed libraries
check:
	./addEOP -c $(ROSE_SOURCE_DIR)/testCode.C

clean:
	rm ./addEOP
	rm ./addEOP.lo







