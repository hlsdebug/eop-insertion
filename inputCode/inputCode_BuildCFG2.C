float foo(int a, float b);

int bar(int a);

int main(int x) {
	int y;

	// Added to allow non-trivial CFG
	if (x)
		y = 2;
	else
		y = 3;

	// Added to allow non-trivial CFG
	int array[10];
	int i;
	for (i=0; i < 10; i++)
		array[i] = y;
	
	if (x)
		y = bar(x);
	else
		y = x + 2;
		//y = bar(x);

	return y;
}

float foo(int a, float b){
	float c=0;
	if (b>0)
		c = b;
	return c;
}

int bar(int a){
	int d;
	if(foo(a,2)>0)
		d = a;
	return d;
}

