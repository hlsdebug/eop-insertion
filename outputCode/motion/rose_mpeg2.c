int eop_75_Buffer_Level_104;
int eop_75_Buffer_Level_103;
int eop_75_Buffer_Level_102;
int eop_75_Buffer_Level_101;
int eop_75_Buffer_Level_96;
int eop_61_n_tmp_65;
int eop_67_k_105;
int eop_67_j_101;
int eop_67_i_100;
int eop_67_k_90;
int eop_67_j_87;
int eop_67_i_84;
int eop_75_Buffer_Level_104;
int eop_75_Buffer_Level_103;
int eop_75_Buffer_Level_102;
int eop_75_Buffer_Level_101;
int eop_75_Buffer_Level_96;
int eop_61_n_tmp_65;
int eop_54_code_61;
unsigned int eop_171_Val_173;
int eop_366_ld_Incnt_160;
int eop_132_Incnt_156;
int eop_132_Incnt_145;
int eop_132_Incnt_136;
int eop_75_Buffer_Level_92;
int eop_75_Buffer_Level_80;
int eop_61_n_tmp_64;
int eop_153_vec_169;
int eop_153_vec_161;
int eop_153_vec_157;
int eop_153_lim_156;
int eop_0_r_size_155;
int eop_106_motion_residual_124;
int eop_105_motion_code_123;
int eop_106_motion_residual_112;
int eop_105_motion_code_110;
int eop_68_main_result_106;
int eop_67_k_105;
int eop_68_main_result_103;
int eop_67_j_101;
int eop_67_i_100;
int eop_67_k_90;
int eop_67_j_87;
int eop_67_i_84;
int eop_72_mvscale_83;
int eop_72_dmv_82;
int eop_72_v_r_size_81;
int eop_72_h_r_size_80;
int eop_72_mv_format_79;
int eop_72_motion_vector_count_78;
int eop_72_s_77;
int eop_361_System_Stream_Flag_76;
int eop_46_evalue_75;
int eop_68_main_result_74;
unsigned int eop_365_ld_Bfr_60;
int eop_366_ld_Incnt_57;
int eop_153_vec_169;
int eop_153_vec_161;
int eop_153_vec_157;
int eop_153_lim_156;
int eop_0_r_size_155;
int eop_106_motion_residual_124;
int eop_105_motion_code_123;
int eop_106_motion_residual_112;
int eop_105_motion_code_110;
int eop_54_code_61;
unsigned int eop_171_Val_173;
int eop_366_ld_Incnt_160;
int eop_132_Incnt_156;
int eop_132_Incnt_145;
int eop_132_Incnt_136;
int eop_75_Buffer_Level_92;
int eop_75_Buffer_Level_80;
int eop_61_n_tmp_64;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
const int inPMV[2][2][2] = {{{(45), (207)}, {(70), (41)}}, {{(4), (180)}, {(120), (216)}}};
const int inmvfs[2][2] = {{(232), (200)}, {(32), (240)}};
const int outPMV[2][2][2] = {{{(1566), (206)}, {(70), (41)}}, {{(1566), (206)}, {(120), (216)}}};
const int outmvfs[2][2] = {{(0), (200)}, {(0), (240)}};
int evalue;
#include "config.h"
#include "global.h"
#include "getbits.c"
#include "getvlc.h"
#include "getvlc.c"
#include "motion.c"

void Initialize_Buffer()
{
  ld_Incnt = 0;
  eop_366_ld_Incnt_57 = 0;
  ld_Rdptr = ld_Rdbfr + 2048;
  ld_Rdmax = ld_Rdptr;
  ld_Bfr = 68157440;
  eop_365_ld_Bfr_60 = ld_Bfr;
/* fills valid data into bfr */
  Flush_Buffer(0);
}

int main()
{
  
#pragma HLS interface port=eop_75_Buffer_Level_104
  
#pragma HLS interface port=eop_75_Buffer_Level_103
  
#pragma HLS interface port=eop_75_Buffer_Level_102
  
#pragma HLS interface port=eop_75_Buffer_Level_101
  
#pragma HLS interface port=eop_75_Buffer_Level_96
  
#pragma HLS interface port=eop_61_n_tmp_65
  
#pragma HLS interface port=eop_67_k_105
  
#pragma HLS interface port=eop_67_j_101
  
#pragma HLS interface port=eop_67_i_100
  
#pragma HLS interface port=eop_67_k_90
  
#pragma HLS interface port=eop_67_j_87
  
#pragma HLS interface port=eop_67_i_84
  
#pragma HLS interface port=eop_75_Buffer_Level_104
  
#pragma HLS interface port=eop_75_Buffer_Level_103
  
#pragma HLS interface port=eop_75_Buffer_Level_102
  
#pragma HLS interface port=eop_75_Buffer_Level_101
  
#pragma HLS interface port=eop_75_Buffer_Level_96
  
#pragma HLS interface port=eop_61_n_tmp_65
  
#pragma HLS interface port=eop_54_code_61
  
#pragma HLS interface port=eop_171_Val_173
  
#pragma HLS interface port=eop_366_ld_Incnt_160
  
#pragma HLS interface port=eop_132_Incnt_156
  
#pragma HLS interface port=eop_132_Incnt_145
  
#pragma HLS interface port=eop_132_Incnt_136
  
#pragma HLS interface port=eop_75_Buffer_Level_92
  
#pragma HLS interface port=eop_75_Buffer_Level_80
  
#pragma HLS interface port=eop_61_n_tmp_64
  
#pragma HLS interface port=eop_153_vec_169
  
#pragma HLS interface port=eop_153_vec_161
  
#pragma HLS interface port=eop_153_vec_157
  
#pragma HLS interface port=eop_153_lim_156
  
#pragma HLS interface port=eop_0_r_size_155
  
#pragma HLS interface port=eop_106_motion_residual_124
  
#pragma HLS interface port=eop_105_motion_code_123
  
#pragma HLS interface port=eop_106_motion_residual_112
  
#pragma HLS interface port=eop_105_motion_code_110
  
#pragma HLS interface port=eop_68_main_result_106
  
#pragma HLS interface port=eop_67_k_105
  
#pragma HLS interface port=eop_68_main_result_103
  
#pragma HLS interface port=eop_67_j_101
  
#pragma HLS interface port=eop_67_i_100
  
#pragma HLS interface port=eop_67_k_90
  
#pragma HLS interface port=eop_67_j_87
  
#pragma HLS interface port=eop_67_i_84
  
#pragma HLS interface port=eop_72_mvscale_83
  
#pragma HLS interface port=eop_72_dmv_82
  
#pragma HLS interface port=eop_72_v_r_size_81
  
#pragma HLS interface port=eop_72_h_r_size_80
  
#pragma HLS interface port=eop_72_mv_format_79
  
#pragma HLS interface port=eop_72_motion_vector_count_78
  
#pragma HLS interface port=eop_72_s_77
  
#pragma HLS interface port=eop_361_System_Stream_Flag_76
  
#pragma HLS interface port=eop_46_evalue_75
  
#pragma HLS interface port=eop_68_main_result_74
  
#pragma HLS interface port=eop_365_ld_Bfr_60
  
#pragma HLS interface port=eop_366_ld_Incnt_57
  
#pragma HLS interface port=eop_153_vec_169
  
#pragma HLS interface port=eop_153_vec_161
  
#pragma HLS interface port=eop_153_vec_157
  
#pragma HLS interface port=eop_153_lim_156
  
#pragma HLS interface port=eop_0_r_size_155
  
#pragma HLS interface port=eop_106_motion_residual_124
  
#pragma HLS interface port=eop_105_motion_code_123
  
#pragma HLS interface port=eop_106_motion_residual_112
  
#pragma HLS interface port=eop_105_motion_code_110
  
#pragma HLS interface port=eop_54_code_61
  
#pragma HLS interface port=eop_171_Val_173
  
#pragma HLS interface port=eop_366_ld_Incnt_160
  
#pragma HLS interface port=eop_132_Incnt_156
  
#pragma HLS interface port=eop_132_Incnt_145
  
#pragma HLS interface port=eop_132_Incnt_136
  
#pragma HLS interface port=eop_75_Buffer_Level_92
  
#pragma HLS interface port=eop_75_Buffer_Level_80
  
#pragma HLS interface port=eop_61_n_tmp_64
  int i;
  int j;
  int k;
  int main_result;
  int PMV[2][2][2];
  int dmvector[2];
  int motion_vertical_field_select[2][2];
  int s;
  int motion_vector_count;
  int mv_format;
  int h_r_size;
  int v_r_size;
  int dmv;
  int mvscale;
  main_result = 0;
  eop_68_main_result_74 = 0;
  evalue = 0;
  eop_46_evalue_75 = 0;
  System_Stream_Flag = 0;
  eop_361_System_Stream_Flag_76 = 0;
  s = 0;
  eop_72_s_77 = 0;
  motion_vector_count = 1;
  eop_72_motion_vector_count_78 = 1;
  mv_format = 0;
  eop_72_mv_format_79 = 0;
  h_r_size = 200;
  eop_72_h_r_size_80 = 200;
  v_r_size = 200;
  eop_72_v_r_size_81 = 200;
  dmv = 0;
  eop_72_dmv_82 = 0;
  mvscale = 1;
  eop_72_mvscale_83 = 1;
  for (i = 0; i < 2; i++) {
    dmvector[i] = 0;
    for (j = 0; j < 2; j++) {
      motion_vertical_field_select[i][j] = inmvfs[i][j];
      for (k = 0; k < 2; k++) 
        PMV[i][j][k] = inPMV[i][j][k];
    }
  }
  Initialize_Buffer();
  motion_vectors(PMV,dmvector,motion_vertical_field_select,s,motion_vector_count,mv_format,h_r_size,v_r_size,dmv,mvscale);
  for (i = 0; i < 2; i++) 
    for (j = 0; j < 2; j++) {
      main_result += motion_vertical_field_select[i][j] == outmvfs[i][j];
      eop_68_main_result_103 = main_result;
      for (k = 0; k < 2; k++) {
        main_result += PMV[i][j][k] == outPMV[i][j][k];
        eop_68_main_result_106 = main_result;
      }
    }
  printf("Result: %d\n",main_result);
  if (main_result == 12) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
