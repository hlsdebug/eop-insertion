bits64 eop_246_zSig_310;
int16 eop_245_zExp_301;
int eop_128_i_131;
bits64 eop_246_zSig_310;
int16 eop_245_zExp_301;
bits64 eop_246_zSig_303;
bits64 eop_246_bSig_297;
bits64 eop_246_aSig_296;
int16 eop_245_zExp_295;
flag eop_244_zSign_255;
flag eop_244_bSign_254;
int16 eop_245_bExp_253;
bits64 eop_246_bSig_252;
flag eop_244_aSign_251;
int16 eop_245_aExp_250;
bits64 eop_246_aSig_249;
int16 eop_174_zExp_230;
bits64 eop_174_zSig_227;
int16 eop_178_roundBits_220;
int16 eop_174_zExp_219;
flag eop_177_isTiny_215;
int16 eop_178_roundBits_204;
int16 eop_178_roundIncrement_200;
int16 eop_178_roundIncrement_195;
int16 eop_178_roundIncrement_191;
int16 eop_178_roundIncrement_187;
int16 eop_178_roundIncrement_182;
flag eop_177_roundNearestEven_181;
int8 eop_176_roundingMode_180;
int8 eop_124_shiftCount_126;
flag eop_111_bIsSignalingNaN_116;
flag eop_111_bIsNaN_115;
flag eop_111_aIsSignalingNaN_114;
flag eop_111_aIsNaN_113;
int8 eop_233_shiftCount_244;
int8 eop_233_shiftCount_238;
int8 eop_233_shiftCount_235;
int8 eop_207_shiftCount_220;
int8 eop_207_shiftCount_217;
int8 eop_207_shiftCount_212;
int8 eop_207_shiftCount_209;
bits64 eop_160_rem0_175;
bits64 eop_159_b1_172;
bits64 eop_161_z_166;
bits64 eop_159_b0_165;
bits64 eop_127_z0_141;
bits64 eop_127_z1_140;
bits64 eop_127_z0_138;
bits64 eop_127_zMiddleA_137;
bits64 eop_127_z0_136;
bits64 eop_127_zMiddleB_135;
bits64 eop_127_zMiddleA_134;
bits64 eop_127_z1_133;
bits32 eop_126_bHigh_132;
bits32 eop_126_bLow_131;
bits32 eop_126_aHigh_130;
bits32 eop_126_aLow_129;
bits64 eop_91_z1_93;
bits64 eop_62_z_74;
bits64 eop_62_z_70;
bits64 eop_62_z_66;
int eop_127_main_result_137;
float64 eop_133_result_136;
float64 eop_129_x2_135;
float64 eop_129_x1_134;
int eop_128_i_131;
int eop_127_main_result_130;
bits64 eop_246_zSig_303;
bits64 eop_246_bSig_297;
bits64 eop_246_aSig_296;
int16 eop_245_zExp_295;
flag eop_244_zSign_255;
flag eop_244_bSign_254;
int16 eop_245_bExp_253;
bits64 eop_246_bSig_252;
flag eop_244_aSign_251;
int16 eop_245_aExp_250;
bits64 eop_246_aSig_249;
int16 eop_174_zExp_230;
bits64 eop_174_zSig_227;
int16 eop_178_roundBits_220;
int16 eop_174_zExp_219;
flag eop_177_isTiny_215;
int16 eop_178_roundBits_204;
int16 eop_178_roundIncrement_200;
int16 eop_178_roundIncrement_195;
int16 eop_178_roundIncrement_191;
int16 eop_178_roundIncrement_187;
int16 eop_178_roundIncrement_182;
flag eop_177_roundNearestEven_181;
int8 eop_176_roundingMode_180;
int8 eop_124_shiftCount_126;
flag eop_111_bIsSignalingNaN_116;
flag eop_111_bIsNaN_115;
flag eop_111_aIsSignalingNaN_114;
flag eop_111_aIsNaN_113;
int8 eop_233_shiftCount_244;
int8 eop_233_shiftCount_238;
int8 eop_233_shiftCount_235;
int8 eop_207_shiftCount_220;
int8 eop_207_shiftCount_217;
int8 eop_207_shiftCount_212;
int8 eop_207_shiftCount_209;
bits64 eop_160_rem0_175;
bits64 eop_159_b1_172;
bits64 eop_161_z_166;
bits64 eop_159_b0_165;
bits64 eop_127_z0_141;
bits64 eop_127_z1_140;
bits64 eop_127_z0_138;
bits64 eop_127_zMiddleA_137;
bits64 eop_127_z0_136;
bits64 eop_127_zMiddleB_135;
bits64 eop_127_zMiddleA_134;
bits64 eop_127_z1_133;
bits32 eop_126_bHigh_132;
bits32 eop_126_bLow_131;
bits32 eop_126_aHigh_130;
bits32 eop_126_aLow_129;
bits64 eop_91_z1_93;
bits64 eop_62_z_74;
bits64 eop_62_z_70;
bits64 eop_62_z_66;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "softfloat.c"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     a_input, b_input : input data                                        |
|     z_output : expected output data                                      |
+--------------------------------------------------------------------------+
*/
#define N 22
const float64 a_input[22] = {(0x7FFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x4008000000000000ULL), (0xC008000000000000ULL), (0x4008000000000000ULL), (0xC008000000000000ULL), (0x4000000000000000ULL), (0xC000000000000000ULL), (0x4000000000000000ULL), (0xC000000000000000ULL), (0x3FF0000000000000ULL), (0xBFF0000000000000ULL), (0x3FF0000000000000ULL), (0xBFF0000000000000ULL)
/* nan */
/* inf */
/* inf */
/* inf */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 3.0 */
/* -3.0 */
/* 3.0 */
/* -3.0 */
/* 2.0 */
/* -2.0 */
/* 2.0 */
/* -2.0 */
/* 1.0 */
/* -1.0 */
/* 1.0 */
/* -1.0 */
};
const float64 b_input[22] = {(0x3FF0000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0x4000000000000000ULL), (0xC000000000000000ULL), (0xC000000000000000ULL), (0x4010000000000000ULL), (0x4010000000000000ULL), (0xC010000000000000ULL), (0xC010000000000000ULL), (0x3FF8000000000000ULL), (0x3FF8000000000000ULL), (0xBFF8000000000000ULL), (0xBFF8000000000000ULL)
/* 1.0 */
/* nan */
/* inf */
/* 1.0 */
/* nan */
/* inf */
/* 0.0 */
/* 0.0 */
/* 1.0 */
/* 1.0 */
/* 2.0 */
/* 2.0 */
/* 2.0 */
/* -2.0 */
/* 4.0 */
/* 4.0 */
/* -4.0 */
/* -4.0 */
/* 1.5 */
/* 1.5 */
/* -1.5 */
/* -1.5 */
};
const float64 z_output[22] = {(0x7FFF000000000000ULL), (0x7FF8000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0x7FF8000000000000ULL), (0x0000000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FF8000000000000ULL), (0xBFF8000000000000ULL), (0xBFF8000000000000ULL), (0x3FF8000000000000ULL), (0x3FE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE5555555555555ULL), (0xBFE5555555555555ULL), (0xBFE5555555555555ULL), (0x3FE5555555555555ULL)
/* nan */
/* nan */
/* nan */
/* inf */
/* nan */
/* 0.0 */
/* nan */
/* inf */
/* 0.0 */
/* -0.0 */
/* 1.5 */
/* -1.5 */
/* 1.5 */
/* -1.5 */
/* 0.5 */
/* 5.0 */
/* -5.0 */
/* 0.5 */
/* 0.666667 */
/* -0.666667 */
/* -0.666667 */
/* 0.666667 */
};

int main()
{
  
#pragma HLS interface port=eop_246_zSig_310
  
#pragma HLS interface port=eop_245_zExp_301
  
#pragma HLS interface port=eop_128_i_131
  
#pragma HLS interface port=eop_246_zSig_310
  
#pragma HLS interface port=eop_245_zExp_301
  
#pragma HLS interface port=eop_246_zSig_303
  
#pragma HLS interface port=eop_246_bSig_297
  
#pragma HLS interface port=eop_246_aSig_296
  
#pragma HLS interface port=eop_245_zExp_295
  
#pragma HLS interface port=eop_244_zSign_255
  
#pragma HLS interface port=eop_244_bSign_254
  
#pragma HLS interface port=eop_245_bExp_253
  
#pragma HLS interface port=eop_246_bSig_252
  
#pragma HLS interface port=eop_244_aSign_251
  
#pragma HLS interface port=eop_245_aExp_250
  
#pragma HLS interface port=eop_246_aSig_249
  
#pragma HLS interface port=eop_174_zExp_230
  
#pragma HLS interface port=eop_174_zSig_227
  
#pragma HLS interface port=eop_178_roundBits_220
  
#pragma HLS interface port=eop_174_zExp_219
  
#pragma HLS interface port=eop_177_isTiny_215
  
#pragma HLS interface port=eop_178_roundBits_204
  
#pragma HLS interface port=eop_178_roundIncrement_200
  
#pragma HLS interface port=eop_178_roundIncrement_195
  
#pragma HLS interface port=eop_178_roundIncrement_191
  
#pragma HLS interface port=eop_178_roundIncrement_187
  
#pragma HLS interface port=eop_178_roundIncrement_182
  
#pragma HLS interface port=eop_177_roundNearestEven_181
  
#pragma HLS interface port=eop_176_roundingMode_180
  
#pragma HLS interface port=eop_124_shiftCount_126
  
#pragma HLS interface port=eop_111_bIsSignalingNaN_116
  
#pragma HLS interface port=eop_111_bIsNaN_115
  
#pragma HLS interface port=eop_111_aIsSignalingNaN_114
  
#pragma HLS interface port=eop_111_aIsNaN_113
  
#pragma HLS interface port=eop_233_shiftCount_244
  
#pragma HLS interface port=eop_233_shiftCount_238
  
#pragma HLS interface port=eop_233_shiftCount_235
  
#pragma HLS interface port=eop_207_shiftCount_220
  
#pragma HLS interface port=eop_207_shiftCount_217
  
#pragma HLS interface port=eop_207_shiftCount_212
  
#pragma HLS interface port=eop_207_shiftCount_209
  
#pragma HLS interface port=eop_160_rem0_175
  
#pragma HLS interface port=eop_159_b1_172
  
#pragma HLS interface port=eop_161_z_166
  
#pragma HLS interface port=eop_159_b0_165
  
#pragma HLS interface port=eop_127_z0_141
  
#pragma HLS interface port=eop_127_z1_140
  
#pragma HLS interface port=eop_127_z0_138
  
#pragma HLS interface port=eop_127_zMiddleA_137
  
#pragma HLS interface port=eop_127_z0_136
  
#pragma HLS interface port=eop_127_zMiddleB_135
  
#pragma HLS interface port=eop_127_zMiddleA_134
  
#pragma HLS interface port=eop_127_z1_133
  
#pragma HLS interface port=eop_126_bHigh_132
  
#pragma HLS interface port=eop_126_bLow_131
  
#pragma HLS interface port=eop_126_aHigh_130
  
#pragma HLS interface port=eop_126_aLow_129
  
#pragma HLS interface port=eop_91_z1_93
  
#pragma HLS interface port=eop_62_z_74
  
#pragma HLS interface port=eop_62_z_70
  
#pragma HLS interface port=eop_62_z_66
  
#pragma HLS interface port=eop_127_main_result_137
  
#pragma HLS interface port=eop_133_result_136
  
#pragma HLS interface port=eop_129_x2_135
  
#pragma HLS interface port=eop_129_x1_134
  
#pragma HLS interface port=eop_128_i_131
  
#pragma HLS interface port=eop_127_main_result_130
  
#pragma HLS interface port=eop_246_zSig_303
  
#pragma HLS interface port=eop_246_bSig_297
  
#pragma HLS interface port=eop_246_aSig_296
  
#pragma HLS interface port=eop_245_zExp_295
  
#pragma HLS interface port=eop_244_zSign_255
  
#pragma HLS interface port=eop_244_bSign_254
  
#pragma HLS interface port=eop_245_bExp_253
  
#pragma HLS interface port=eop_246_bSig_252
  
#pragma HLS interface port=eop_244_aSign_251
  
#pragma HLS interface port=eop_245_aExp_250
  
#pragma HLS interface port=eop_246_aSig_249
  
#pragma HLS interface port=eop_174_zExp_230
  
#pragma HLS interface port=eop_174_zSig_227
  
#pragma HLS interface port=eop_178_roundBits_220
  
#pragma HLS interface port=eop_174_zExp_219
  
#pragma HLS interface port=eop_177_isTiny_215
  
#pragma HLS interface port=eop_178_roundBits_204
  
#pragma HLS interface port=eop_178_roundIncrement_200
  
#pragma HLS interface port=eop_178_roundIncrement_195
  
#pragma HLS interface port=eop_178_roundIncrement_191
  
#pragma HLS interface port=eop_178_roundIncrement_187
  
#pragma HLS interface port=eop_178_roundIncrement_182
  
#pragma HLS interface port=eop_177_roundNearestEven_181
  
#pragma HLS interface port=eop_176_roundingMode_180
  
#pragma HLS interface port=eop_124_shiftCount_126
  
#pragma HLS interface port=eop_111_bIsSignalingNaN_116
  
#pragma HLS interface port=eop_111_bIsNaN_115
  
#pragma HLS interface port=eop_111_aIsSignalingNaN_114
  
#pragma HLS interface port=eop_111_aIsNaN_113
  
#pragma HLS interface port=eop_233_shiftCount_244
  
#pragma HLS interface port=eop_233_shiftCount_238
  
#pragma HLS interface port=eop_233_shiftCount_235
  
#pragma HLS interface port=eop_207_shiftCount_220
  
#pragma HLS interface port=eop_207_shiftCount_217
  
#pragma HLS interface port=eop_207_shiftCount_212
  
#pragma HLS interface port=eop_207_shiftCount_209
  
#pragma HLS interface port=eop_160_rem0_175
  
#pragma HLS interface port=eop_159_b1_172
  
#pragma HLS interface port=eop_161_z_166
  
#pragma HLS interface port=eop_159_b0_165
  
#pragma HLS interface port=eop_127_z0_141
  
#pragma HLS interface port=eop_127_z1_140
  
#pragma HLS interface port=eop_127_z0_138
  
#pragma HLS interface port=eop_127_zMiddleA_137
  
#pragma HLS interface port=eop_127_z0_136
  
#pragma HLS interface port=eop_127_zMiddleB_135
  
#pragma HLS interface port=eop_127_zMiddleA_134
  
#pragma HLS interface port=eop_127_z1_133
  
#pragma HLS interface port=eop_126_bHigh_132
  
#pragma HLS interface port=eop_126_bLow_131
  
#pragma HLS interface port=eop_126_aHigh_130
  
#pragma HLS interface port=eop_126_aLow_129
  
#pragma HLS interface port=eop_91_z1_93
  
#pragma HLS interface port=eop_62_z_74
  
#pragma HLS interface port=eop_62_z_70
  
#pragma HLS interface port=eop_62_z_66
  int main_result;
  int i;
  float64 x1;
  float64 x2;
  main_result = 0;
  eop_127_main_result_130 = 0;
  for (i = 0; i < 22; i++) {
    float64 result;
    x1 = a_input[i];
    eop_129_x1_134 = x1;
    x2 = b_input[i];
    eop_129_x2_135 = x2;
    result = float64_div(x1,x2);
    eop_133_result_136 = result;
    main_result += result == z_output[i];
    eop_127_main_result_137 = main_result;
    printf("a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n",a_input[i],b_input[i],z_output[i],result);
  }
  printf("Result: %d\n",main_result);
  if (main_result == 22) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
