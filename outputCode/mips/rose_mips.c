int eop_104_pc_104;
int eop_103_Lo_103;
int eop_102_Hi_102;
int eop_106_j_298;
int eop_120_i_132;
int eop_120_i_126;
int eop_38_main_result_300;
int eop_106_j_298;
int eop_38_main_result_297;
int eop_121_n_inst_293;
int eop_104_pc_287;
int eop_104_pc_275;
int eop_104_pc_271;
int eop_104_pc_267;
int eop_110_rs_237;
int eop_111_rt_236;
short eop_115_address_235;
int eop_104_pc_230;
int eop_116_tgtadr_228;
int eop_104_pc_225;
int eop_116_tgtadr_224;
int eop_104_pc_218;
int eop_104_pc_215;
int eop_102_Hi_175;
int eop_103_Lo_174;
long long eop_100_hilo_171;
int eop_102_Hi_168;
int eop_103_Lo_167;
long long eop_100_hilo_166;
int eop_110_rs_153;
int eop_111_rt_152;
int eop_112_rd_151;
int eop_113_shamt_150;
int eop_114_funct_149;
int eop_109_op_144;
int eop_104_pc_142;
unsigned int eop_108_ins_141;
int eop_104_pc_137;
int eop_120_i_132;
int eop_120_i_126;
int eop_38_main_result_124;
int eop_121_n_inst_123;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
* Copyright (C) 2008
* Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
* Nagoya University, Japan
* All rights reserved.
*
* Disclaimer of Warranty
*
* These software programs are available to the user without any license fee or
* royalty on an "as is" basis. The authors disclaims any and all warranties, 
* whether express, implied, or statuary, including any implied warranties or 
* merchantability or of fitness for a particular purpose. In no event shall the
* copyright-holder be liable for any incidental, punitive, or consequential damages
* of any kind whatsoever arising from the use of these programs. This disclaimer
* of warranty extends to the user of these programs and user's customers, employees,
* agents, transferees, successors, and assigns.
*
*/
#include <stdio.h>
int main_result;
#define R 0
#define ADDU 33
#define SUBU 35
#define MULT 24
#define MULTU 25
#define MFHI 16
#define MFLO 18
#define AND 36
#define OR 37
#define XOR 38
#define SLL 0
#define SRL 2
#define SLLV 4
#define SRLV 6
#define SLT 42
#define SLTU 43
#define JR 8
#define J 2
#define JAL 3
#define ADDIU 9
#define ANDI 12
#define ORI 13
#define XORI 14
#define LW 35
#define SW 43
#define LUI 15
#define BEQ 4
#define BNE 5
#define BGEZ 1
#define SLTI 10
#define SLTIU 11
#include "imem.h"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     A : input data                                                       |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const int A[8] = {(22), (5), (- 9), (3), (- 17), (38), (0), (11)};
const int outData[8] = {(- 17), (- 9), (0), (3), (5), (11), (22), (38)};
#define IADDR(x)	(((x)&0x000000ff)>>2)
#define DADDR(x)	(((x)&0x000000ff)>>2)

int main()
{
  
#pragma HLS interface port=eop_104_pc_104
  
#pragma HLS interface port=eop_103_Lo_103
  
#pragma HLS interface port=eop_102_Hi_102
  
#pragma HLS interface port=eop_106_j_298
  
#pragma HLS interface port=eop_120_i_132
  
#pragma HLS interface port=eop_120_i_126
  
#pragma HLS interface port=eop_38_main_result_300
  
#pragma HLS interface port=eop_106_j_298
  
#pragma HLS interface port=eop_38_main_result_297
  
#pragma HLS interface port=eop_121_n_inst_293
  
#pragma HLS interface port=eop_104_pc_287
  
#pragma HLS interface port=eop_104_pc_275
  
#pragma HLS interface port=eop_104_pc_271
  
#pragma HLS interface port=eop_104_pc_267
  
#pragma HLS interface port=eop_110_rs_237
  
#pragma HLS interface port=eop_111_rt_236
  
#pragma HLS interface port=eop_115_address_235
  
#pragma HLS interface port=eop_104_pc_230
  
#pragma HLS interface port=eop_116_tgtadr_228
  
#pragma HLS interface port=eop_104_pc_225
  
#pragma HLS interface port=eop_116_tgtadr_224
  
#pragma HLS interface port=eop_104_pc_218
  
#pragma HLS interface port=eop_104_pc_215
  
#pragma HLS interface port=eop_102_Hi_175
  
#pragma HLS interface port=eop_103_Lo_174
  
#pragma HLS interface port=eop_100_hilo_171
  
#pragma HLS interface port=eop_102_Hi_168
  
#pragma HLS interface port=eop_103_Lo_167
  
#pragma HLS interface port=eop_100_hilo_166
  
#pragma HLS interface port=eop_110_rs_153
  
#pragma HLS interface port=eop_111_rt_152
  
#pragma HLS interface port=eop_112_rd_151
  
#pragma HLS interface port=eop_113_shamt_150
  
#pragma HLS interface port=eop_114_funct_149
  
#pragma HLS interface port=eop_109_op_144
  
#pragma HLS interface port=eop_104_pc_142
  
#pragma HLS interface port=eop_108_ins_141
  
#pragma HLS interface port=eop_104_pc_137
  
#pragma HLS interface port=eop_120_i_132
  
#pragma HLS interface port=eop_120_i_126
  
#pragma HLS interface port=eop_38_main_result_124
  
#pragma HLS interface port=eop_121_n_inst_123
  long long hilo;
  int reg[32];
  int Hi = 0;
  eop_102_Hi_102 = Hi;
  int Lo = 0;
  eop_103_Lo_103 = Lo;
  int pc = 0;
  eop_104_pc_104 = pc;
  int dmem[64] = {(0)};
  int j;
  unsigned int ins;
  int op;
  int rs;
  int rt;
  int rd;
  int shamt;
  int funct;
  short address;
  int tgtadr;
  while(1){
    int i;
    int n_inst;
    n_inst = 0;
    eop_121_n_inst_123 = 0;
    main_result = 0;
    eop_38_main_result_124 = 0;
    for (i = 0; i < 32; i++) {
      reg[i] = 0;
    }
    reg[29] = 0x7fffeffc;
    for (i = 0; i < 8; i++) {
      dmem[i] = A[i];
    }
    pc = 0x00400000;
    eop_104_pc_137 = 0x00400000;
    do {
      ins = imem[(pc & 0x000000ff) >> 2];
      eop_108_ins_141 = ins;
      pc = pc + 4;
      eop_104_pc_142 = pc;
      op = (ins >> 26);
      eop_109_op_144 = op;
      switch(op){
        case 0:
{
          funct = (ins & 0x3f);
          eop_114_funct_149 = funct;
          shamt = (ins >> 6 & 0x1f);
          eop_113_shamt_150 = shamt;
          rd = (ins >> 11 & 0x1f);
          eop_112_rd_151 = rd;
          rt = (ins >> 16 & 0x1f);
          eop_111_rt_152 = rt;
          rs = (ins >> 21 & 0x1f);
          eop_110_rs_153 = rs;
          switch(funct){
            case 33:
{
              reg[rd] = reg[rs] + reg[rt];
              break; 
            }
            case 35:
{
              reg[rd] = reg[rs] - reg[rt];
              break; 
            }
            case 24:
{
              hilo = ((long long )reg[rs]) * ((long long )reg[rt]);
              eop_100_hilo_166 = hilo;
              Lo = (hilo & 0x00000000ffffffffULL);
              eop_103_Lo_167 = Lo;
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              eop_102_Hi_168 = Hi;
              break; 
            }
            case 25:
{
              hilo = (((unsigned long long )reg[rs]) * ((unsigned long long )reg[rt]));
              eop_100_hilo_171 = hilo;
              Lo = (hilo & 0x00000000ffffffffULL);
              eop_103_Lo_174 = Lo;
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              eop_102_Hi_175 = Hi;
              break; 
            }
            case 16:
{
              reg[rd] = Hi;
              break; 
            }
            case 18:
{
              reg[rd] = Lo;
              break; 
            }
            case 36:
{
              reg[rd] = reg[rs] & reg[rt];
              break; 
            }
            case 37:
{
              reg[rd] = reg[rs] | reg[rt];
              break; 
            }
            case 38:
{
              reg[rd] = reg[rs] ^ reg[rt];
              break; 
            }
            case 0:
{
              reg[rd] = reg[rt] << shamt;
              break; 
            }
            case 2:
{
              reg[rd] = reg[rt] >> shamt;
              break; 
            }
            case 4:
{
              reg[rd] = reg[rt] << reg[rs];
              break; 
            }
            case 6:
{
              reg[rd] = reg[rt] >> reg[rs];
              break; 
            }
            case 42:
{
              reg[rd] = reg[rs] < reg[rt];
              break; 
            }
            case 43:
{
              reg[rd] = ((unsigned int )reg[rs]) < ((unsigned int )reg[rt]);
              break; 
            }
            case 8:
{
              pc = reg[rs];
              eop_104_pc_215 = pc;
              break; 
            }
            default:
{
// error
              pc = 0;
              eop_104_pc_218 = 0;
              break; 
            }
          }
          break; 
        }
        case 2:
{
          tgtadr = (ins & 0x3ffffff);
          eop_116_tgtadr_224 = tgtadr;
          pc = tgtadr << 2;
          eop_104_pc_225 = pc;
          break; 
        }
        case 3:
{
          tgtadr = (ins & 0x3ffffff);
          eop_116_tgtadr_228 = tgtadr;
          reg[31] = pc;
          pc = tgtadr << 2;
          eop_104_pc_230 = pc;
          break; 
        }
        default:
{
          address = (ins & 0xffff);
          eop_115_address_235 = address;
          rt = (ins >> 16 & 0x1f);
          eop_111_rt_236 = rt;
          rs = (ins >> 21 & 0x1f);
          eop_110_rs_237 = rs;
          switch(op){
            case 9:
{
              reg[rt] = reg[rs] + address;
              break; 
            }
            case 12:
{
              reg[rt] = reg[rs] & ((unsigned short )address);
              break; 
            }
            case 13:
{
              reg[rt] = reg[rs] | ((unsigned short )address);
              break; 
            }
            case 14:
{
              reg[rt] = reg[rs] ^ ((unsigned short )address);
              break; 
            }
            case 35:
{
              reg[rt] = dmem[(reg[rs] + address & 0x000000ff) >> 2];
              break; 
            }
            case 43:
{
              dmem[(reg[rs] + address & 0x000000ff) >> 2] = reg[rt];
              break; 
            }
            case 15:
{
              reg[rt] = address << 16;
              break; 
            }
            case 4:
{
              if (reg[rs] == reg[rt]) {
                pc = pc - 4 + (address << 2);
                eop_104_pc_267 = pc;
              }
              break; 
            }
            case 5:
{
              if (reg[rs] != reg[rt]) {
                pc = pc - 4 + (address << 2);
                eop_104_pc_271 = pc;
              }
              break; 
            }
            case 1:
{
              if (reg[rs] >= 0) {
                pc = pc - 4 + (address << 2);
                eop_104_pc_275 = pc;
              }
              break; 
            }
            case 10:
{
              reg[rt] = reg[rs] < address;
              break; 
            }
            case 11:
{
              reg[rt] = ((unsigned int )reg[rs]) < ((unsigned short )address);
              break; 
            }
            default:
{
/* error */
              pc = 0;
              eop_104_pc_287 = 0;
              break; 
            }
          }
          break; 
        }
      }
      reg[0] = 0;
      n_inst = n_inst + 1;
      eop_121_n_inst_293 = n_inst;
    }while (pc != 0);
    main_result += n_inst == 611;
    eop_38_main_result_297 = main_result;
    for (j = 0; j < 8; j++) {
      main_result += dmem[j] == outData[j];
      eop_38_main_result_300 = main_result;
    }
    printf("Result: %d\n",main_result);
    if (main_result == 9) {
      printf("RESULT: PASS\n");
    }
     else {
      printf("RESULT: FAIL\n");
    }
    return main_result;
  }
}
