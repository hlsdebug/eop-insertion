int16 eop_245_zExp_297;
int eop_121_i_124;
int16 eop_245_zExp_297;
bits64 eop_246_bSig_291;
bits64 eop_246_aSig_290;
int16 eop_245_zExp_289;
flag eop_244_zSign_254;
flag eop_244_bSign_253;
int16 eop_245_bExp_252;
bits64 eop_246_bSig_251;
flag eop_244_aSign_250;
int16 eop_245_aExp_249;
bits64 eop_246_aSig_248;
int16 eop_174_zExp_230;
bits64 eop_174_zSig_227;
int16 eop_178_roundBits_220;
int16 eop_174_zExp_219;
flag eop_177_isTiny_215;
int16 eop_178_roundBits_204;
int16 eop_178_roundIncrement_200;
int16 eop_178_roundIncrement_195;
int16 eop_178_roundIncrement_191;
int16 eop_178_roundIncrement_187;
int16 eop_178_roundIncrement_182;
flag eop_177_roundNearestEven_181;
int8 eop_176_roundingMode_180;
int8 eop_124_shiftCount_126;
flag eop_109_bIsSignalingNaN_114;
flag eop_109_bIsNaN_113;
flag eop_109_aIsSignalingNaN_112;
flag eop_109_aIsNaN_111;
int8 eop_162_shiftCount_173;
int8 eop_162_shiftCount_167;
int8 eop_162_shiftCount_164;
int8 eop_136_shiftCount_149;
int8 eop_136_shiftCount_146;
int8 eop_136_shiftCount_141;
int8 eop_136_shiftCount_138;
bits64 eop_90_z0_104;
bits64 eop_90_z1_103;
bits64 eop_90_z0_101;
bits64 eop_90_zMiddleA_100;
bits64 eop_90_z0_99;
bits64 eop_90_zMiddleB_98;
bits64 eop_90_zMiddleA_97;
bits64 eop_90_z1_96;
bits32 eop_89_bHigh_95;
bits32 eop_89_bLow_94;
bits32 eop_89_aHigh_93;
bits32 eop_89_aLow_92;
bits64 eop_62_z_74;
bits64 eop_62_z_70;
bits64 eop_62_z_66;
int eop_120_main_result_130;
float64 eop_126_result_129;
float64 eop_122_x2_128;
float64 eop_122_x1_127;
int eop_121_i_124;
int eop_120_main_result_123;
bits64 eop_246_bSig_291;
bits64 eop_246_aSig_290;
int16 eop_245_zExp_289;
flag eop_244_zSign_254;
flag eop_244_bSign_253;
int16 eop_245_bExp_252;
bits64 eop_246_bSig_251;
flag eop_244_aSign_250;
int16 eop_245_aExp_249;
bits64 eop_246_aSig_248;
int16 eop_174_zExp_230;
bits64 eop_174_zSig_227;
int16 eop_178_roundBits_220;
int16 eop_174_zExp_219;
flag eop_177_isTiny_215;
int16 eop_178_roundBits_204;
int16 eop_178_roundIncrement_200;
int16 eop_178_roundIncrement_195;
int16 eop_178_roundIncrement_191;
int16 eop_178_roundIncrement_187;
int16 eop_178_roundIncrement_182;
flag eop_177_roundNearestEven_181;
int8 eop_176_roundingMode_180;
int8 eop_124_shiftCount_126;
flag eop_109_bIsSignalingNaN_114;
flag eop_109_bIsNaN_113;
flag eop_109_aIsSignalingNaN_112;
flag eop_109_aIsNaN_111;
int8 eop_162_shiftCount_173;
int8 eop_162_shiftCount_167;
int8 eop_162_shiftCount_164;
int8 eop_136_shiftCount_149;
int8 eop_136_shiftCount_146;
int8 eop_136_shiftCount_141;
int8 eop_136_shiftCount_138;
bits64 eop_90_z0_104;
bits64 eop_90_z1_103;
bits64 eop_90_z0_101;
bits64 eop_90_zMiddleA_100;
bits64 eop_90_z0_99;
bits64 eop_90_zMiddleB_98;
bits64 eop_90_zMiddleA_97;
bits64 eop_90_z1_96;
bits32 eop_89_bHigh_95;
bits32 eop_89_bLow_94;
bits32 eop_89_aHigh_93;
bits32 eop_89_aLow_92;
bits64 eop_62_z_74;
bits64 eop_62_z_70;
bits64 eop_62_z_66;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "softfloat.c"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     a_input, b_input : input data                                        |
|     z_output : expected output data                                      |
+--------------------------------------------------------------------------+
*/
#define N 20
const float64 a_input[20] = {(0x7FF0000000000000ULL), (0x7FFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0x3FD0000000000000ULL), (0x0000000000000000ULL)
/* inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 0.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 1.0 */
/* 1.0 */
/* 2.0 */
/* 0.25 */
/* -2.0 */
/* -0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* 0.0 */
};
const float64 b_input[20] = {(0xFFFFFFFFFFFFFFFFULL), (0xFFF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* -inf */
/* nan */
/* -inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* -2.0 */
/* 0.0 */
};
const float64 z_output[20] = {(0xFFFFFFFFFFFFFFFFULL), (0x7FFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* nan */
/* nan */
/* inf */
/* nan */
/* nan */
/* inf */
/* 0.0 */
/* -0.0 */
/* 0.0 */
/* -0.0 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* 0.0 */
};

int main()
{
  
#pragma HLS interface port=eop_245_zExp_297
  
#pragma HLS interface port=eop_121_i_124
  
#pragma HLS interface port=eop_245_zExp_297
  
#pragma HLS interface port=eop_246_bSig_291
  
#pragma HLS interface port=eop_246_aSig_290
  
#pragma HLS interface port=eop_245_zExp_289
  
#pragma HLS interface port=eop_244_zSign_254
  
#pragma HLS interface port=eop_244_bSign_253
  
#pragma HLS interface port=eop_245_bExp_252
  
#pragma HLS interface port=eop_246_bSig_251
  
#pragma HLS interface port=eop_244_aSign_250
  
#pragma HLS interface port=eop_245_aExp_249
  
#pragma HLS interface port=eop_246_aSig_248
  
#pragma HLS interface port=eop_174_zExp_230
  
#pragma HLS interface port=eop_174_zSig_227
  
#pragma HLS interface port=eop_178_roundBits_220
  
#pragma HLS interface port=eop_174_zExp_219
  
#pragma HLS interface port=eop_177_isTiny_215
  
#pragma HLS interface port=eop_178_roundBits_204
  
#pragma HLS interface port=eop_178_roundIncrement_200
  
#pragma HLS interface port=eop_178_roundIncrement_195
  
#pragma HLS interface port=eop_178_roundIncrement_191
  
#pragma HLS interface port=eop_178_roundIncrement_187
  
#pragma HLS interface port=eop_178_roundIncrement_182
  
#pragma HLS interface port=eop_177_roundNearestEven_181
  
#pragma HLS interface port=eop_176_roundingMode_180
  
#pragma HLS interface port=eop_124_shiftCount_126
  
#pragma HLS interface port=eop_109_bIsSignalingNaN_114
  
#pragma HLS interface port=eop_109_bIsNaN_113
  
#pragma HLS interface port=eop_109_aIsSignalingNaN_112
  
#pragma HLS interface port=eop_109_aIsNaN_111
  
#pragma HLS interface port=eop_162_shiftCount_173
  
#pragma HLS interface port=eop_162_shiftCount_167
  
#pragma HLS interface port=eop_162_shiftCount_164
  
#pragma HLS interface port=eop_136_shiftCount_149
  
#pragma HLS interface port=eop_136_shiftCount_146
  
#pragma HLS interface port=eop_136_shiftCount_141
  
#pragma HLS interface port=eop_136_shiftCount_138
  
#pragma HLS interface port=eop_90_z0_104
  
#pragma HLS interface port=eop_90_z1_103
  
#pragma HLS interface port=eop_90_z0_101
  
#pragma HLS interface port=eop_90_zMiddleA_100
  
#pragma HLS interface port=eop_90_z0_99
  
#pragma HLS interface port=eop_90_zMiddleB_98
  
#pragma HLS interface port=eop_90_zMiddleA_97
  
#pragma HLS interface port=eop_90_z1_96
  
#pragma HLS interface port=eop_89_bHigh_95
  
#pragma HLS interface port=eop_89_bLow_94
  
#pragma HLS interface port=eop_89_aHigh_93
  
#pragma HLS interface port=eop_89_aLow_92
  
#pragma HLS interface port=eop_62_z_74
  
#pragma HLS interface port=eop_62_z_70
  
#pragma HLS interface port=eop_62_z_66
  
#pragma HLS interface port=eop_120_main_result_130
  
#pragma HLS interface port=eop_126_result_129
  
#pragma HLS interface port=eop_122_x2_128
  
#pragma HLS interface port=eop_122_x1_127
  
#pragma HLS interface port=eop_121_i_124
  
#pragma HLS interface port=eop_120_main_result_123
  
#pragma HLS interface port=eop_246_bSig_291
  
#pragma HLS interface port=eop_246_aSig_290
  
#pragma HLS interface port=eop_245_zExp_289
  
#pragma HLS interface port=eop_244_zSign_254
  
#pragma HLS interface port=eop_244_bSign_253
  
#pragma HLS interface port=eop_245_bExp_252
  
#pragma HLS interface port=eop_246_bSig_251
  
#pragma HLS interface port=eop_244_aSign_250
  
#pragma HLS interface port=eop_245_aExp_249
  
#pragma HLS interface port=eop_246_aSig_248
  
#pragma HLS interface port=eop_174_zExp_230
  
#pragma HLS interface port=eop_174_zSig_227
  
#pragma HLS interface port=eop_178_roundBits_220
  
#pragma HLS interface port=eop_174_zExp_219
  
#pragma HLS interface port=eop_177_isTiny_215
  
#pragma HLS interface port=eop_178_roundBits_204
  
#pragma HLS interface port=eop_178_roundIncrement_200
  
#pragma HLS interface port=eop_178_roundIncrement_195
  
#pragma HLS interface port=eop_178_roundIncrement_191
  
#pragma HLS interface port=eop_178_roundIncrement_187
  
#pragma HLS interface port=eop_178_roundIncrement_182
  
#pragma HLS interface port=eop_177_roundNearestEven_181
  
#pragma HLS interface port=eop_176_roundingMode_180
  
#pragma HLS interface port=eop_124_shiftCount_126
  
#pragma HLS interface port=eop_109_bIsSignalingNaN_114
  
#pragma HLS interface port=eop_109_bIsNaN_113
  
#pragma HLS interface port=eop_109_aIsSignalingNaN_112
  
#pragma HLS interface port=eop_109_aIsNaN_111
  
#pragma HLS interface port=eop_162_shiftCount_173
  
#pragma HLS interface port=eop_162_shiftCount_167
  
#pragma HLS interface port=eop_162_shiftCount_164
  
#pragma HLS interface port=eop_136_shiftCount_149
  
#pragma HLS interface port=eop_136_shiftCount_146
  
#pragma HLS interface port=eop_136_shiftCount_141
  
#pragma HLS interface port=eop_136_shiftCount_138
  
#pragma HLS interface port=eop_90_z0_104
  
#pragma HLS interface port=eop_90_z1_103
  
#pragma HLS interface port=eop_90_z0_101
  
#pragma HLS interface port=eop_90_zMiddleA_100
  
#pragma HLS interface port=eop_90_z0_99
  
#pragma HLS interface port=eop_90_zMiddleB_98
  
#pragma HLS interface port=eop_90_zMiddleA_97
  
#pragma HLS interface port=eop_90_z1_96
  
#pragma HLS interface port=eop_89_bHigh_95
  
#pragma HLS interface port=eop_89_bLow_94
  
#pragma HLS interface port=eop_89_aHigh_93
  
#pragma HLS interface port=eop_89_aLow_92
  
#pragma HLS interface port=eop_62_z_74
  
#pragma HLS interface port=eop_62_z_70
  
#pragma HLS interface port=eop_62_z_66
  int main_result;
  int i;
  float64 x1;
  float64 x2;
  main_result = 0;
  eop_120_main_result_123 = 0;
  for (i = 0; i < 20; i++) {
    float64 result;
    x1 = a_input[i];
    eop_122_x1_127 = x1;
    x2 = b_input[i];
    eop_122_x2_128 = x2;
    result = float64_mul(x1,x2);
    eop_126_result_129 = result;
    main_result += result == z_output[i];
    eop_120_main_result_130 = main_result;
    printf("a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n",a_input[i],b_input[i],z_output[i],result);
  }
  printf("Result: %d\n",main_result);
  if (main_result == 20) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
