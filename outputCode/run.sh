#!/bin/bash



cd adpcm
../../addEOP ../../chstone/adpcm/adpcm.c &> log.log
cd ../aes
../../addEOP ../../chstone/aes/aes.c ../../chstone/aes/aes_dec.c ../../chstone/aes/aes_enc.c ../../chstone/aes/aes_func.c ../../chstone/aes/aes_key.c &> log.log
cd ../blowfish
../../addEOP ../../chstone/blowfish/bf.c ../../chstone/blowfish/bf_cfb64.c ../../chstone/blowfish/bf_enc.c ../../chstone/blowfish/bf_skey.c &> log.log
cd ../dfadd
../../addEOP ../../chstone/dfadd/dfadd.c ../../chstone/dfadd/softfloat.c &> log.log
cd ../dfdiv
../../addEOP ../../chstone/dfdiv/dfdiv.c ../../chstone/dfdiv/softfloat.c &> log.log
cd ../dfmul
../../addEOP ../../chstone/dfmul/dfmul.c ../../chstone/dfmul/softfloat.c &> log.log
cd ../dfsin
../../addEOP ../../chstone/dfsin/dfsin.c ../../chstone/dfsin/softfloat.c &> log.log
cd ../gsm
../../addEOP ../../chstone/gsm/gsm.c ../../chstone/gsm/add.c ../../chstone/gsm/lpc.c &> log.log
cd ../jpeg
../../addEOP ../../chstone/jpeg/main.c ../../chstone/jpeg/chenidct.c ../../chstone/jpeg/decode.c ../../chstone/jpeg/huffman.c ../../chstone/jpeg/jpeg2bmp.c ../../chstone/jpeg/jfif_read.c ../../chstone/jpeg/marker.c &> log.log
cd ../mips
../../addEOP ../../chstone/mips/mips.c &> log.log
cd ../motion
../../addEOP ../../chstone/motion/mpeg2.c ../../chstone/motion/motion.c ../../chstone/motion/getbits.c ../../chstone/motion/getvlc.c &> log.log
cd ../sha
../../addEOP ../../chstone/sha/sha_driver.c ../../chstone/sha/sha.c &> log.log
