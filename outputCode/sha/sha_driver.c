volatile int eop_208_j_212;
volatile int eop_181_count_188;
volatile LONG eop_33_sha_info_count_hi_162;
volatile int eop_100_i_129;
volatile int eop_100_i_125;
volatile int eop_100_i_121;
volatile int eop_100_i_117;
volatile int eop_100_i_107;
volatile int eop_100_i_103;
volatile int eop_78_m_83;
volatile int eop_57_m_66;
volatile int eop_53_e_62;
volatile int eop_43_i_48;
volatile int eop_208_j_212;
volatile int eop_181_count_188;
volatile LONG eop_33_sha_info_count_hi_162;
volatile int eop_100_i_129;
volatile int eop_100_i_125;
volatile int eop_100_i_121;
volatile int eop_100_i_117;
volatile int eop_100_i_107;
volatile int eop_100_i_103;
volatile int eop_78_m_83;
volatile int eop_57_m_66;
volatile int eop_53_e_62;
volatile int eop_208_i_214;
volatile int eop_208_j_212;
volatile int eop_181_count_187;
volatile LONG eop_183_hi_bit_count_186;
volatile LONG eop_182_lo_bit_count_185;
volatile LONG eop_33_sha_info_count_hi_165;
volatile LONG eop_33_sha_info_count_lo_164;
volatile LONG eop_33_sha_info_count_hi_152;
volatile LONG eop_33_sha_info_count_lo_151;
volatile LONG eop_101_A_131;
volatile LONG eop_101_B_131;
volatile LONG eop_101_C_131;
volatile LONG eop_101_D_131;
volatile LONG eop_101_E_131;
volatile LONG eop_101_temp_131;
volatile int eop_100_i_129;
volatile LONG eop_101_A_127;
volatile LONG eop_101_B_127;
volatile LONG eop_101_C_127;
volatile LONG eop_101_D_127;
volatile LONG eop_101_E_127;
volatile LONG eop_101_temp_127;
volatile int eop_100_i_125;
volatile LONG eop_101_A_123;
volatile LONG eop_101_B_123;
volatile LONG eop_101_C_123;
volatile LONG eop_101_D_123;
volatile LONG eop_101_E_123;
volatile LONG eop_101_temp_123;
volatile int eop_100_i_121;
volatile LONG eop_101_A_119;
volatile LONG eop_101_B_119;
volatile LONG eop_101_C_119;
volatile LONG eop_101_D_119;
volatile LONG eop_101_E_119;
volatile LONG eop_101_temp_119;
volatile int eop_100_i_117;
volatile LONG eop_101_E_115;
volatile LONG eop_101_D_114;
volatile LONG eop_101_C_113;
volatile LONG eop_101_B_112;
volatile LONG eop_101_A_111;
volatile int eop_100_i_107;
volatile int eop_100_i_103;
volatile LONG eop_77_tmp_85;
volatile int eop_78_m_79;
volatile LONG eop_55_uc_60;
volatile int eop_57_m_59;
volatile int eop_44_main_result_51;
volatile int eop_43_i_48;
volatile int eop_44_main_result_45;
volatile int eop_208_i_214;
volatile int eop_208_j_212;
volatile int eop_181_count_187;
volatile LONG eop_183_hi_bit_count_186;
volatile LONG eop_182_lo_bit_count_185;
volatile LONG eop_33_sha_info_count_hi_165;
volatile LONG eop_33_sha_info_count_lo_164;
volatile LONG eop_33_sha_info_count_hi_152;
volatile LONG eop_33_sha_info_count_lo_151;
volatile LONG eop_101_A_131;
volatile LONG eop_101_B_131;
volatile LONG eop_101_C_131;
volatile LONG eop_101_D_131;
volatile LONG eop_101_E_131;
volatile LONG eop_101_temp_131;
volatile int eop_100_i_129;
volatile LONG eop_101_A_127;
volatile LONG eop_101_B_127;
volatile LONG eop_101_C_127;
volatile LONG eop_101_D_127;
volatile LONG eop_101_E_127;
volatile LONG eop_101_temp_127;
volatile int eop_100_i_125;
volatile LONG eop_101_A_123;
volatile LONG eop_101_B_123;
volatile LONG eop_101_C_123;
volatile LONG eop_101_D_123;
volatile LONG eop_101_E_123;
volatile LONG eop_101_temp_123;
volatile int eop_100_i_121;
volatile LONG eop_101_A_119;
volatile LONG eop_101_B_119;
volatile LONG eop_101_C_119;
volatile LONG eop_101_D_119;
volatile LONG eop_101_E_119;
volatile LONG eop_101_temp_119;
volatile int eop_100_i_117;
volatile LONG eop_101_E_115;
volatile LONG eop_101_D_114;
volatile LONG eop_101_C_113;
volatile LONG eop_101_B_112;
volatile LONG eop_101_A_111;
volatile int eop_100_i_107;
volatile int eop_100_i_103;
volatile LONG eop_77_tmp_85;
volatile int eop_78_m_79;
volatile LONG eop_55_uc_60;
volatile int eop_57_m_59;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
#include <stdio.h>
#include "sha.h"
#include "sha.c"
/*
+--------------------------------------------------------------------------+
| * Test Vector (added for CHStone)                                        |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const LONG outData[5] = {(0x006a5a37UL), (0x93dc9485UL), (0x2c412112UL), (0x63f7ba43UL), (0xad73f922UL)};

int main()
{
  int i;
  int main_result;
  main_result = 0;
  eop_44_main_result_45 = 0;
  sha_stream();
  for (i = 0; i < 5; i++) {
    main_result += sha_info_digest[i] == outData[i];
    eop_44_main_result_51 = main_result;
  }
  printf("Result: %d\n",main_result);
  if (main_result == 5) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
