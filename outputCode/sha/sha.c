/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
/* SHA f()-functions */
#define f1(x,y,z)	((x & y) | (~x & z))
#define f2(x,y,z)	(x ^ y ^ z)
#define f3(x,y,z)	((x & y) | (x & z) | (y & z))
#define f4(x,y,z)	(x ^ y ^ z)
/* SHA constants */
#define CONST1		0x5a827999L
#define CONST2		0x6ed9eba1L
#define CONST3		0x8f1bbcdcL
#define CONST4		0xca62c1d6L
/* 32-bit rotate */
#define ROT32(x,n)	((x << n) | (x >> (32 - n)))
#define FUNC(n,i)						\
    temp = ROT32(A,5) + f##n(B,C,D) + E + W[i] + CONST##n;	\
    E = D; D = C; C = ROT32(B,30); B = A; A = temp
#include "sha.h"

void memset(LONG *s,int c,int n,int e)
{
  LONG uc;
  LONG *p;
  int m;
  m = n / 4;
  eop_57_m_59 = m;
  uc = c;
  eop_55_uc_60 = uc;
  p = ((LONG *)s);
  while(e-- > 0){
    p++;
  }
  eop_53_e_62 = e;
  while(m-- > 0){
     *(p++) = uc;
  }
  eop_57_m_66 = m;
}

void memcpy(LONG *s1,const BYTE *s2,int n)
{
  LONG *p1;
  BYTE *p2;
  LONG tmp;
  int m;
  m = n / 4;
  eop_78_m_79 = m;
  p1 = ((LONG *)s1);
  p2 = ((BYTE *)s2);
  while(m-- > 0){
    tmp = 0;
    eop_77_tmp_85 = tmp;
    tmp |= (0xFF & ( *(p2++)));
    tmp |= ((0xFF & ( *(p2++))) << 8);
    tmp |= ((0xFF & ( *(p2++))) << 16);
    tmp |= ((0xFF & ( *(p2++))) << 24);
     *p1 = tmp;
    p1++;
  }
  eop_78_m_83 = m;
}
/* do SHA transformation */

static void sha_transform()
{
  int i;
  LONG temp;
  LONG A;
  LONG B;
  LONG C;
  LONG D;
  LONG E;
  LONG W[80];
  for (i = 0; i < 16; ++i) {
    W[i] = sha_info_data[i];
  }
  for (i = 16; i < 80; ++i) {
    W[i] = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
  }
  A = sha_info_digest[0];
  eop_101_A_111 = A;
  B = sha_info_digest[1];
  eop_101_B_112 = B;
  C = sha_info_digest[2];
  eop_101_C_113 = C;
  D = sha_info_digest[3];
  eop_101_D_114 = D;
  E = sha_info_digest[4];
  eop_101_E_115 = E;
  for (i = 0; i < 20; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B & C | ~B & D) + E + W[i]) + 0x5a827999L);
    eop_101_temp_119 = temp;
    E = D;
    eop_101_E_119 = E;
    D = C;
    eop_101_D_119 = D;
    C = B << 30 | B >> 32 - 30;
    eop_101_C_119 = C;
    B = A;
    eop_101_B_119 = B;
    A = temp;
    eop_101_A_119 = A;
  }
  for (i = 20; i < 40; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0x6ed9eba1L);
    eop_101_temp_123 = temp;
    E = D;
    eop_101_E_123 = E;
    D = C;
    eop_101_D_123 = D;
    C = B << 30 | B >> 32 - 30;
    eop_101_C_123 = C;
    B = A;
    eop_101_B_123 = B;
    A = temp;
    eop_101_A_123 = A;
  }
  for (i = 40; i < 60; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B & C | B & D | C & D) + E + W[i]) + 0x8f1bbcdcL);
    eop_101_temp_127 = temp;
    E = D;
    eop_101_E_127 = E;
    D = C;
    eop_101_D_127 = D;
    C = B << 30 | B >> 32 - 30;
    eop_101_C_127 = C;
    B = A;
    eop_101_B_127 = B;
    A = temp;
    eop_101_A_127 = A;
  }
  for (i = 60; i < 80; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0xca62c1d6L);
    eop_101_temp_131 = temp;
    E = D;
    eop_101_E_131 = E;
    D = C;
    eop_101_D_131 = D;
    C = B << 30 | B >> 32 - 30;
    eop_101_C_131 = C;
    B = A;
    eop_101_B_131 = B;
    A = temp;
    eop_101_A_131 = A;
  }
  sha_info_digest[0] += A;
  sha_info_digest[1] += B;
  sha_info_digest[2] += C;
  sha_info_digest[3] += D;
  sha_info_digest[4] += E;
}
/* initialize the SHA digest */

void sha_init()
{
  sha_info_digest[0] = 0x67452301L;
  sha_info_digest[1] = 0xefcdab89L;
  sha_info_digest[2] = 0x98badcfeL;
  sha_info_digest[3] = 0x10325476L;
  sha_info_digest[4] = 0xc3d2e1f0L;
  sha_info_count_lo = 0L;
  eop_33_sha_info_count_lo_151 = sha_info_count_lo;
  sha_info_count_hi = 0L;
  eop_33_sha_info_count_hi_152 = sha_info_count_hi;
}
/* update the SHA digest */

void sha_update(const BYTE *buffer,int count)
{
  if (sha_info_count_lo + (((LONG )count) << 3) < sha_info_count_lo) {
    ++sha_info_count_hi;
    eop_33_sha_info_count_hi_162 = sha_info_count_hi;
  }
  sha_info_count_lo += ((LONG )count) << 3;
  eop_33_sha_info_count_lo_164 = sha_info_count_lo;
  sha_info_count_hi += ((LONG )count) >> 29;
  eop_33_sha_info_count_hi_165 = sha_info_count_hi;
  while(count >= 64){
    memcpy(sha_info_data,buffer,64);
    sha_transform();
    buffer += 64;
    count -= 64;
  }
  memcpy(sha_info_data,buffer,count);
}
/* finish computing the SHA digest */

void sha_final()
{
  int count;
  LONG lo_bit_count;
  LONG hi_bit_count;
  lo_bit_count = sha_info_count_lo;
  eop_182_lo_bit_count_185 = lo_bit_count;
  hi_bit_count = sha_info_count_hi;
  eop_183_hi_bit_count_186 = hi_bit_count;
  count = ((int )(lo_bit_count >> 3 & 0x3f));
  eop_181_count_187 = count;
  sha_info_data[count++] = 0x80;
  eop_181_count_188 = count;
  if (count > 56) {
    memset(sha_info_data,0,64 - count,count);
    sha_transform();
    memset(sha_info_data,0,56,0);
  }
   else {
    memset(sha_info_data,0,56 - count,count);
  }
  sha_info_data[14] = hi_bit_count;
  sha_info_data[15] = lo_bit_count;
  sha_transform();
}
/* compute the SHA digest of a FILE stream */

void sha_stream()
{
  int i;
  int j;
  const BYTE *p;
  sha_init();
  for (j = 0; j < 2; j++) {
    i = in_i[j];
    eop_208_i_214 = i;
    p = &indata[j][0];
    sha_update(p,i);
  }
  sha_final();
}
