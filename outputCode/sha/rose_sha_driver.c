int eop_208_j_212;
int eop_181_count_188;
LONG eop_33_sha_info_count_hi_162;
int eop_100_i_129;
int eop_100_i_125;
int eop_100_i_121;
int eop_100_i_117;
int eop_100_i_107;
int eop_100_i_103;
int eop_78_m_83;
int eop_57_m_66;
int eop_53_e_62;
int eop_43_i_48;
int eop_208_j_212;
int eop_181_count_188;
LONG eop_33_sha_info_count_hi_162;
int eop_100_i_129;
int eop_100_i_125;
int eop_100_i_121;
int eop_100_i_117;
int eop_100_i_107;
int eop_100_i_103;
int eop_78_m_83;
int eop_57_m_66;
int eop_53_e_62;
int eop_208_i_214;
int eop_208_j_212;
int eop_181_count_187;
LONG eop_183_hi_bit_count_186;
LONG eop_182_lo_bit_count_185;
LONG eop_33_sha_info_count_hi_165;
LONG eop_33_sha_info_count_lo_164;
LONG eop_33_sha_info_count_hi_152;
LONG eop_33_sha_info_count_lo_151;
LONG eop_101_A_131;
LONG eop_101_B_131;
LONG eop_101_C_131;
LONG eop_101_D_131;
LONG eop_101_E_131;
LONG eop_101_temp_131;
int eop_100_i_129;
LONG eop_101_A_127;
LONG eop_101_B_127;
LONG eop_101_C_127;
LONG eop_101_D_127;
LONG eop_101_E_127;
LONG eop_101_temp_127;
int eop_100_i_125;
LONG eop_101_A_123;
LONG eop_101_B_123;
LONG eop_101_C_123;
LONG eop_101_D_123;
LONG eop_101_E_123;
LONG eop_101_temp_123;
int eop_100_i_121;
LONG eop_101_A_119;
LONG eop_101_B_119;
LONG eop_101_C_119;
LONG eop_101_D_119;
LONG eop_101_E_119;
LONG eop_101_temp_119;
int eop_100_i_117;
LONG eop_101_E_115;
LONG eop_101_D_114;
LONG eop_101_C_113;
LONG eop_101_B_112;
LONG eop_101_A_111;
int eop_100_i_107;
int eop_100_i_103;
LONG eop_77_tmp_85;
int eop_78_m_79;
LONG eop_55_uc_60;
int eop_57_m_59;
int eop_44_main_result_51;
int eop_43_i_48;
int eop_44_main_result_45;
int eop_208_i_214;
int eop_208_j_212;
int eop_181_count_187;
LONG eop_183_hi_bit_count_186;
LONG eop_182_lo_bit_count_185;
LONG eop_33_sha_info_count_hi_165;
LONG eop_33_sha_info_count_lo_164;
LONG eop_33_sha_info_count_hi_152;
LONG eop_33_sha_info_count_lo_151;
LONG eop_101_A_131;
LONG eop_101_B_131;
LONG eop_101_C_131;
LONG eop_101_D_131;
LONG eop_101_E_131;
LONG eop_101_temp_131;
int eop_100_i_129;
LONG eop_101_A_127;
LONG eop_101_B_127;
LONG eop_101_C_127;
LONG eop_101_D_127;
LONG eop_101_E_127;
LONG eop_101_temp_127;
int eop_100_i_125;
LONG eop_101_A_123;
LONG eop_101_B_123;
LONG eop_101_C_123;
LONG eop_101_D_123;
LONG eop_101_E_123;
LONG eop_101_temp_123;
int eop_100_i_121;
LONG eop_101_A_119;
LONG eop_101_B_119;
LONG eop_101_C_119;
LONG eop_101_D_119;
LONG eop_101_E_119;
LONG eop_101_temp_119;
int eop_100_i_117;
LONG eop_101_E_115;
LONG eop_101_D_114;
LONG eop_101_C_113;
LONG eop_101_B_112;
LONG eop_101_A_111;
int eop_100_i_107;
int eop_100_i_103;
LONG eop_77_tmp_85;
int eop_78_m_79;
LONG eop_55_uc_60;
int eop_57_m_59;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
#include <stdio.h>
#include "sha.h"
#include "sha.c"
/*
+--------------------------------------------------------------------------+
| * Test Vector (added for CHStone)                                        |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const LONG outData[5] = {(0x006a5a37UL), (0x93dc9485UL), (0x2c412112UL), (0x63f7ba43UL), (0xad73f922UL)};

int main()
{
  
#pragma HLS interface port=eop_208_j_212
  
#pragma HLS interface port=eop_181_count_188
  
#pragma HLS interface port=eop_33_sha_info_count_hi_162
  
#pragma HLS interface port=eop_100_i_129
  
#pragma HLS interface port=eop_100_i_125
  
#pragma HLS interface port=eop_100_i_121
  
#pragma HLS interface port=eop_100_i_117
  
#pragma HLS interface port=eop_100_i_107
  
#pragma HLS interface port=eop_100_i_103
  
#pragma HLS interface port=eop_78_m_83
  
#pragma HLS interface port=eop_57_m_66
  
#pragma HLS interface port=eop_53_e_62
  
#pragma HLS interface port=eop_43_i_48
  
#pragma HLS interface port=eop_208_j_212
  
#pragma HLS interface port=eop_181_count_188
  
#pragma HLS interface port=eop_33_sha_info_count_hi_162
  
#pragma HLS interface port=eop_100_i_129
  
#pragma HLS interface port=eop_100_i_125
  
#pragma HLS interface port=eop_100_i_121
  
#pragma HLS interface port=eop_100_i_117
  
#pragma HLS interface port=eop_100_i_107
  
#pragma HLS interface port=eop_100_i_103
  
#pragma HLS interface port=eop_78_m_83
  
#pragma HLS interface port=eop_57_m_66
  
#pragma HLS interface port=eop_53_e_62
  
#pragma HLS interface port=eop_208_i_214
  
#pragma HLS interface port=eop_208_j_212
  
#pragma HLS interface port=eop_181_count_187
  
#pragma HLS interface port=eop_183_hi_bit_count_186
  
#pragma HLS interface port=eop_182_lo_bit_count_185
  
#pragma HLS interface port=eop_33_sha_info_count_hi_165
  
#pragma HLS interface port=eop_33_sha_info_count_lo_164
  
#pragma HLS interface port=eop_33_sha_info_count_hi_152
  
#pragma HLS interface port=eop_33_sha_info_count_lo_151
  
#pragma HLS interface port=eop_101_A_131
  
#pragma HLS interface port=eop_101_B_131
  
#pragma HLS interface port=eop_101_C_131
  
#pragma HLS interface port=eop_101_D_131
  
#pragma HLS interface port=eop_101_E_131
  
#pragma HLS interface port=eop_101_temp_131
  
#pragma HLS interface port=eop_100_i_129
  
#pragma HLS interface port=eop_101_A_127
  
#pragma HLS interface port=eop_101_B_127
  
#pragma HLS interface port=eop_101_C_127
  
#pragma HLS interface port=eop_101_D_127
  
#pragma HLS interface port=eop_101_E_127
  
#pragma HLS interface port=eop_101_temp_127
  
#pragma HLS interface port=eop_100_i_125
  
#pragma HLS interface port=eop_101_A_123
  
#pragma HLS interface port=eop_101_B_123
  
#pragma HLS interface port=eop_101_C_123
  
#pragma HLS interface port=eop_101_D_123
  
#pragma HLS interface port=eop_101_E_123
  
#pragma HLS interface port=eop_101_temp_123
  
#pragma HLS interface port=eop_100_i_121
  
#pragma HLS interface port=eop_101_A_119
  
#pragma HLS interface port=eop_101_B_119
  
#pragma HLS interface port=eop_101_C_119
  
#pragma HLS interface port=eop_101_D_119
  
#pragma HLS interface port=eop_101_E_119
  
#pragma HLS interface port=eop_101_temp_119
  
#pragma HLS interface port=eop_100_i_117
  
#pragma HLS interface port=eop_101_E_115
  
#pragma HLS interface port=eop_101_D_114
  
#pragma HLS interface port=eop_101_C_113
  
#pragma HLS interface port=eop_101_B_112
  
#pragma HLS interface port=eop_101_A_111
  
#pragma HLS interface port=eop_100_i_107
  
#pragma HLS interface port=eop_100_i_103
  
#pragma HLS interface port=eop_77_tmp_85
  
#pragma HLS interface port=eop_78_m_79
  
#pragma HLS interface port=eop_55_uc_60
  
#pragma HLS interface port=eop_57_m_59
  
#pragma HLS interface port=eop_44_main_result_51
  
#pragma HLS interface port=eop_43_i_48
  
#pragma HLS interface port=eop_44_main_result_45
  
#pragma HLS interface port=eop_208_i_214
  
#pragma HLS interface port=eop_208_j_212
  
#pragma HLS interface port=eop_181_count_187
  
#pragma HLS interface port=eop_183_hi_bit_count_186
  
#pragma HLS interface port=eop_182_lo_bit_count_185
  
#pragma HLS interface port=eop_33_sha_info_count_hi_165
  
#pragma HLS interface port=eop_33_sha_info_count_lo_164
  
#pragma HLS interface port=eop_33_sha_info_count_hi_152
  
#pragma HLS interface port=eop_33_sha_info_count_lo_151
  
#pragma HLS interface port=eop_101_A_131
  
#pragma HLS interface port=eop_101_B_131
  
#pragma HLS interface port=eop_101_C_131
  
#pragma HLS interface port=eop_101_D_131
  
#pragma HLS interface port=eop_101_E_131
  
#pragma HLS interface port=eop_101_temp_131
  
#pragma HLS interface port=eop_100_i_129
  
#pragma HLS interface port=eop_101_A_127
  
#pragma HLS interface port=eop_101_B_127
  
#pragma HLS interface port=eop_101_C_127
  
#pragma HLS interface port=eop_101_D_127
  
#pragma HLS interface port=eop_101_E_127
  
#pragma HLS interface port=eop_101_temp_127
  
#pragma HLS interface port=eop_100_i_125
  
#pragma HLS interface port=eop_101_A_123
  
#pragma HLS interface port=eop_101_B_123
  
#pragma HLS interface port=eop_101_C_123
  
#pragma HLS interface port=eop_101_D_123
  
#pragma HLS interface port=eop_101_E_123
  
#pragma HLS interface port=eop_101_temp_123
  
#pragma HLS interface port=eop_100_i_121
  
#pragma HLS interface port=eop_101_A_119
  
#pragma HLS interface port=eop_101_B_119
  
#pragma HLS interface port=eop_101_C_119
  
#pragma HLS interface port=eop_101_D_119
  
#pragma HLS interface port=eop_101_E_119
  
#pragma HLS interface port=eop_101_temp_119
  
#pragma HLS interface port=eop_100_i_117
  
#pragma HLS interface port=eop_101_E_115
  
#pragma HLS interface port=eop_101_D_114
  
#pragma HLS interface port=eop_101_C_113
  
#pragma HLS interface port=eop_101_B_112
  
#pragma HLS interface port=eop_101_A_111
  
#pragma HLS interface port=eop_100_i_107
  
#pragma HLS interface port=eop_100_i_103
  
#pragma HLS interface port=eop_77_tmp_85
  
#pragma HLS interface port=eop_78_m_79
  
#pragma HLS interface port=eop_55_uc_60
  
#pragma HLS interface port=eop_57_m_59
  int i;
  int main_result;
  main_result = 0;
  eop_44_main_result_45 = 0;
  sha_stream();
  for (i = 0; i < 5; i++) {
    main_result += sha_info_digest[i] == outData[i];
    eop_44_main_result_51 = main_result;
  }
  printf("Result: %d\n",main_result);
  if (main_result == 5) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
