/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*============================================================================
This C source file is part of the SoftFloat IEC/IEEE Floating-point Arithmetic
Package, Release 2b.
Written by John R. Hauser.  This work was made possible in part by the
International Computer Science Institute, located at Suite 600, 1947 Center
Street, Berkeley, California 94704.  Funding was partially provided by the
National Science Foundation under grant MIP-9311980.  The original version
of this code was written as part of a project to build a fixed-point vector
processor in collaboration with the University of California at Berkeley,
overseen by Profs. Nelson Morgan and John Wawrzynek.  More information
is available through the Web page `http://www.cs.berkeley.edu/~jhauser/
arithmetic/SoftFloat.html'.
THIS SOFTWARE IS DISTRIBUTED AS IS, FOR FREE.  Although reasonable effort has
been made to avoid it, THIS SOFTWARE MAY CONTAIN FAULTS THAT WILL AT TIMES
RESULT IN INCORRECT BEHAVIOR.  USE OF THIS SOFTWARE IS RESTRICTED TO PERSONS
AND ORGANIZATIONS WHO CAN AND WILL TAKE FULL RESPONSIBILITY FOR ALL LOSSES,
COSTS, OR OTHER PROBLEMS THEY INCUR DUE TO THE SOFTWARE, AND WHO FURTHERMORE
EFFECTIVELY INDEMNIFY JOHN HAUSER AND THE INTERNATIONAL COMPUTER SCIENCE
INSTITUTE (possibly via similar legal warning) AGAINST ALL LOSSES, COSTS, OR
OTHER PROBLEMS INCURRED BY THEIR CUSTOMERS AND CLIENTS DUE TO THE SOFTWARE.
Derivative works are acceptable, even for commercial purposes, so long as
(1) the source code for the derivative work includes prominent notice that
the work is derivative, and (2) the source code includes prominent notice with
these four paragraphs for those parts of this code that are retained.
=============================================================================*/
#include "milieu.h"
#include "softfloat.h"
/*----------------------------------------------------------------------------
| Floating-point rounding mode, extended double-precision rounding precision,
| and exception flags.
*----------------------------------------------------------------------------*/
//int8 float_rounding_mode = float_round_nearest_even;
#define float_rounding_mode float_round_nearest_even
//int float_exception_flags = 0;
/*----------------------------------------------------------------------------
| Primitive arithmetic functions, including multi-word arithmetic, and
| division and square root approximations.  (Can be specialized to target if
| desired.)
*----------------------------------------------------------------------------*/
#include "softfloat-macros"
/*----------------------------------------------------------------------------
| Functions and definitions to determine:  (1) whether tininess for underflow
| is detected before or after rounding by default, (2) what (if anything)
| happens when exceptions are raised, (3) how signaling NaNs are distinguished
| from quiet NaNs, (4) the default generated quiet NaNs, and (5) how NaNs
| are propagated from function inputs to output.  These details are target-
| specific.
*----------------------------------------------------------------------------*/
#include "softfloat-specialize"
/*----------------------------------------------------------------------------
| Returns the fraction bits of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

bits64 extractFloat64Frac(float64 a)
{
  return a & 0x000FFFFFFFFFFFFFLL;
}
/*----------------------------------------------------------------------------
| Returns the exponent bits of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

int16 extractFloat64Exp(float64 a)
{
  return (a >> 52 & 0x7FF);
}
/*----------------------------------------------------------------------------
| Returns the sign bit of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

flag extractFloat64Sign(float64 a)
{
  return (a >> 63);
}
/*----------------------------------------------------------------------------
| Packs the sign `zSign', exponent `zExp', and significand `zSig' into a
| double-precision floating-point value, returning the result.  After being
| shifted into the proper positions, the three fields are simply added
| together to form the result.  This means that any integer portion of `zSig'
| will be added into the exponent.  Since a properly normalized significand
| will have an integer portion equal to 1, the `zExp' input should be 1 less
| than the desired result exponent whenever `zSig' is a complete, normalized
| significand.
*----------------------------------------------------------------------------*/

float64 packFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  return (((bits64 )zSign) << 63) + (((bits64 )zExp) << 52) + zSig;
}
/*----------------------------------------------------------------------------
| Takes an abstract floating-point value having sign `zSign', exponent `zExp',
| and significand `zSig', and returns the proper double-precision floating-
| point value corresponding to the abstract input.  Ordinarily, the abstract
| value is simply rounded and packed into the double-precision format, with
| the inexact exception raised if the abstract input cannot be represented
| exactly.  However, if the abstract value is too large, the overflow and
| inexact exceptions are raised and an infinity or maximal finite value is
| returned.  If the abstract value is too small, the input value is rounded
| to a subnormal number, and the underflow and inexact exceptions are raised
| if the abstract input cannot be represented exactly as a subnormal double-
| precision floating-point number.
|     The input significand `zSig' has its binary point between bits 62
| and 61, which is 10 bits to the left of the usual location.  This shifted
| significand must be normalized or smaller.  If `zSig' is not normalized,
| `zExp' must be 0; in that case, the result returned is a subnormal number,
| and it must not require rounding.  In the usual case that `zSig' is
| normalized, `zExp' must be 1 less than the ``true'' floating-point exponent.
| The handling of underflow and overflow follows the IEC/IEEE Standard for
| Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 roundAndPackFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  int8 roundingMode;
  flag roundNearestEven;
  flag isTiny;
  int16 roundIncrement;
  int16 roundBits;
  roundingMode = 0;
  eop_158_roundingMode_162 = 0;
  roundNearestEven = roundingMode == 0;
  eop_159_roundNearestEven_163 = roundNearestEven;
  roundIncrement = 0x200;
  eop_160_roundIncrement_164 = 0x200;
  if (!roundNearestEven) {
    if (roundingMode == 1) {
      roundIncrement = 0;
      eop_160_roundIncrement_169 = 0;
    }
     else {
      roundIncrement = 0x3FF;
      eop_160_roundIncrement_173 = 0x3FF;
      if (zSign) {
        if (roundingMode == 2) {
          roundIncrement = 0;
          eop_160_roundIncrement_177 = 0;
        }
      }
       else {
        if (roundingMode == 3) {
          roundIncrement = 0;
          eop_160_roundIncrement_182 = 0;
        }
      }
    }
  }
  roundBits = (zSig & 0x3FF);
  eop_160_roundBits_186 = roundBits;
  if (0x7FD <= ((bits16 )zExp)) {
    if (0x7FD < zExp || zExp == 0x7FD && ((sbits64 )(zSig + roundIncrement)) < 0) {
//float_raise (float_flag_overflow | float_flag_inexact);
      return packFloat64(zSign,0x7FF,0) - (roundIncrement == 0);
    }
    if (zExp < 0) {
      isTiny = 1 == 1 || zExp < - 1 || zSig + roundIncrement < 0x8000000000000000LL;
      eop_159_isTiny_197 = isTiny;
      shift64RightJamming(zSig,-zExp,&zSig);
      zExp = 0;
      eop_156_zExp_201 = 0;
      roundBits = (zSig & 0x3FF);
      eop_160_roundBits_202 = roundBits;
//if (isTiny && roundBits)
//float_raise (float_flag_underflow);
    }
  }
//if (roundBits)
//float_exception_flags |= float_flag_inexact;
  zSig = zSig + roundIncrement >> 10;
  eop_156_zSig_209 = zSig;
  zSig &= (~((roundBits ^ 0x200) == 0 & roundNearestEven));
  if (zSig == 0) {
    zExp = 0;
    eop_156_zExp_212 = 0;
  }
  return packFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Takes an abstract floating-point value having sign `zSign', exponent `zExp',
| and significand `zSig', and returns the proper double-precision floating-
| point value corresponding to the abstract input.  This routine is just like
| `roundAndPackFloat64' except that `zSig' does not have to be normalized.
| Bit 63 of `zSig' must be zero, and `zExp' must be 1 less than the ``true''
| floating-point exponent.
*----------------------------------------------------------------------------*/

static float64 normalizeRoundAndPackFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  int8 shiftCount;
  shiftCount = countLeadingZeros64(zSig) - 1;
  eop_229_shiftCount_231 = shiftCount;
  return roundAndPackFloat64(zSign,zExp - shiftCount,zSig << shiftCount);
}
/*----------------------------------------------------------------------------
| Returns the result of adding the absolute values of the double-precision
| floating-point values `a' and `b'.  If `zSign' is 1, the sum is negated
| before being returned.  `zSign' is ignored if the result is a NaN.
| The addition is performed according to the IEC/IEEE Standard for Binary
| Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 addFloat64Sigs(float64 a,float64 b,flag zSign)
{
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig;
  int16 expDiff;
  aSig = extractFloat64Frac(a);
  eop_248_aSig_251 = aSig;
  aExp = extractFloat64Exp(a);
  eop_247_aExp_252 = aExp;
  bSig = extractFloat64Frac(b);
  eop_248_bSig_253 = bSig;
  bExp = extractFloat64Exp(b);
  eop_247_bExp_254 = bExp;
  expDiff = aExp - bExp;
  eop_249_expDiff_255 = expDiff;
  aSig <<= 9;
  bSig <<= 9;
  if (0 < expDiff) {
    if (aExp == 0x7FF) {
      if (aSig) 
        return propagateFloat64NaN(a,b);
      return a;
    }
    if (bExp == 0) {
      --expDiff;
      eop_249_expDiff_267 = expDiff;
    }
     else 
      bSig |= 0x2000000000000000LL;
    shift64RightJamming(bSig,expDiff,&bSig);
    zExp = aExp;
    eop_247_zExp_271 = zExp;
  }
   else if (expDiff < 0) {
    if (bExp == 0x7FF) {
      if (bSig) 
        return propagateFloat64NaN(a,b);
      return packFloat64(zSign,0x7FF,0);
    }
    if (aExp == 0) {
      ++expDiff;
      eop_249_expDiff_282 = expDiff;
    }
     else {
      aSig |= 0x2000000000000000LL;
    }
    shift64RightJamming(aSig,-expDiff,&aSig);
    zExp = bExp;
    eop_247_zExp_288 = zExp;
  }
   else {
    if (aExp == 0x7FF) {
      if (aSig | bSig) 
        return propagateFloat64NaN(a,b);
      return a;
    }
    if (aExp == 0) 
      return packFloat64(zSign,0,aSig + bSig >> 9);
    zSig = 0x4000000000000000LL + aSig + bSig;
    eop_248_zSig_300 = zSig;
    zExp = aExp;
    eop_247_zExp_301 = zExp;
    goto roundAndPack;
  }
  aSig |= 0x2000000000000000LL;
  zSig = aSig + bSig << 1;
  eop_248_zSig_305 = zSig;
  --zExp;
  eop_247_zExp_306 = zExp;
  if (((sbits64 )zSig) < 0) {
    zSig = aSig + bSig;
    eop_248_zSig_309 = zSig;
    ++zExp;
    eop_247_zExp_310 = zExp;
  }
  roundAndPack:
  return roundAndPackFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Returns the result of subtracting the absolute values of the double-
| precision floating-point values `a' and `b'.  If `zSign' is 1, the
| difference is negated before being returned.  `zSign' is ignored if the
| result is a NaN.  The subtraction is performed according to the IEC/IEEE
| Standard for Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 subFloat64Sigs(float64 a,float64 b,flag zSign)
{
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig;
  int16 expDiff;
  aSig = extractFloat64Frac(a);
  eop_329_aSig_332 = aSig;
  aExp = extractFloat64Exp(a);
  eop_328_aExp_333 = aExp;
  bSig = extractFloat64Frac(b);
  eop_329_bSig_334 = bSig;
  bExp = extractFloat64Exp(b);
  eop_328_bExp_335 = bExp;
  expDiff = aExp - bExp;
  eop_330_expDiff_336 = expDiff;
  aSig <<= 10;
  bSig <<= 10;
  if (0 < expDiff) 
    goto aExpBigger;
  if (expDiff < 0) 
    goto bExpBigger;
  if (aExp == 0x7FF) {
    if (aSig | bSig) 
      return propagateFloat64NaN(a,b);
//float_raise (float_flag_invalid);
    return 0x7FFFFFFFFFFFFFFFLL;
  }
  if (aExp == 0) {
    aExp = 1;
    eop_328_aExp_352 = 1;
    bExp = 1;
    eop_328_bExp_353 = 1;
  }
  if (bSig < aSig) 
    goto aBigger;
  if (aSig < bSig) 
    goto bBigger;
  return packFloat64(0 == 3,0,0);
  bExpBigger:
  if (bExp == 0x7FF) {
    if (bSig) 
      return propagateFloat64NaN(a,b);
    return packFloat64(zSign ^ 1,0x7FF,0);
  }
  if (aExp == 0) {
    ++expDiff;
    eop_330_expDiff_368 = expDiff;
  }
   else 
    aSig |= 0x4000000000000000LL;
  shift64RightJamming(aSig,-expDiff,&aSig);
  bSig |= 0x4000000000000000LL;
  bBigger:
  zSig = bSig - aSig;
  eop_329_zSig_374 = zSig;
  zExp = bExp;
  eop_328_zExp_375 = zExp;
  zSign ^= 1;
  goto normalizeRoundAndPack;
  aExpBigger:
  if (aExp == 0x7FF) {
    if (aSig) 
      return propagateFloat64NaN(a,b);
    return a;
  }
  if (bExp == 0) {
    --expDiff;
    eop_330_expDiff_386 = expDiff;
  }
   else 
    bSig |= 0x4000000000000000LL;
  shift64RightJamming(bSig,expDiff,&bSig);
  aSig |= 0x4000000000000000LL;
  aBigger:
  zSig = aSig - bSig;
  eop_329_zSig_392 = zSig;
  zExp = aExp;
  eop_328_zExp_393 = zExp;
  normalizeRoundAndPack:
  --zExp;
  eop_328_zExp_395 = zExp;
  return normalizeRoundAndPackFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Returns the result of adding the double-precision floating-point values `a'
| and `b'.  The operation is performed according to the IEC/IEEE Standard for
| Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

float64 float64_add(float64 a,float64 b)
{
  flag aSign;
  flag bSign;
  aSign = extractFloat64Sign(a);
  eop_409_aSign_411 = aSign;
  bSign = extractFloat64Sign(b);
  eop_409_bSign_412 = bSign;
  if (aSign == bSign) 
    return addFloat64Sigs(a,b,aSign);
   else 
    return subFloat64Sigs(a,b,aSign);
}
