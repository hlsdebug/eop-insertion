/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>

#include "global.h"
#include "decode.h"
#include "init.h"
#include "marker.c"
#include "chenidct.c"
#include "huffman.h"
#include "decode.c"
#include "huffman.c"
#include "jfif_read.c"
#include "jpeg2bmp.c"

int
main ()
{
	read_position = -1;
	i_marker = 0;
	out_length_get_sof = 17;
	out_data_precision_get_sof = 8;
	out_p_jinfo_image_height_get_sof = 59;
	out_p_jinfo_image_width_get_sof = 90;
	out_p_jinfo_num_components_get_sof = 3;
	i_get_sos = 0;
	out_length_get_sos = 12;
	out_num_comp_get_sos = 3;
	i_get_dht = 0;
	i_get_dqt = 0;


		main_result = 0;
      jpeg2bmp_main ();

      printf ("Result: %d\n", main_result);
      if (main_result == 21745) {
          printf("RESULT: PASS\n");
      } else {
          printf("RESULT: FAIL\n");
      }

      return main_result;
    }
